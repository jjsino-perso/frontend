import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Feature extends Component {
    render() {
       const feature = this.props.data
       const uri = `/features/${feature.id}`
        return (<Link to={uri}>{feature.name}</Link>)
    }
};