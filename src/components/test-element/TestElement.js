import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

export const TEST_STATES = [
    'undefined',
    'passed',
    'failed'
];

export default class TestElement extends Component {

    constructor(props) {
        super(props);

        this.state = this.props.data;

        this.handleDelete = this.handleDelete.bind(this);
        this.handleStatusChange = this.handleStatusChange.bind(this);
    }

    handleDelete() {
        this.props.deleteTest(this.state);
    }

    handleStatusChange(e) {
        this.setState({ value: e.target.value });
        this.props.updateStatus(this.state.id, e.target.value);
    }

    render() {
        return (
            <Row>
                <Col>{this.props.data.name}</Col>
                <Col>
                    <Form.Control as="select" onChange={this.handleStatusChange} value={this.state.status}>
                        {(TEST_STATES).map((state, index) => <option key={index}>{state}</option>)}
                    </Form.Control>
                </Col>
                <Col><Button onClick={this.handleDelete} variant="danger">Delete</Button></Col>
            </Row>
        );
    }
}