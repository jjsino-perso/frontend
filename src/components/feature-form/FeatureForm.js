import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import './FeatureForm.scss';

export default class FeatureForm extends Component {
    constructor(props) {
        super(props);
        this.state = {name: ''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    handleSubmit(e) {
        e.preventDefault();
        e.stopPropagation();
        this.props.createFeature(this.state.name);
    }

    handleChange(e) {
        this.setState({name: e.target.value});
    }

    render() {
        return (
            <Form noValidate onSubmit={this.handleSubmit} className="FeatureForm">
                <Row>
                    <Col>
                        <Form.Control type="text" placeholder="Feature name" value={this.state.name} onChange={this.handleChange}/>
                    </Col>
                    <Col>
                        <Button variant="primary" type="submit">
                            Validate
                        </Button>
                    </Col>
                </Row>
            </Form>
        )
    }
}