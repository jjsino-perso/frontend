import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import Container from 'react-bootstrap/Container';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import 'react-toastify/dist/ReactToastify.css';
import Feature from '../../components/feature/Feature';
import ButtonAdd from '../button/ButtonAdd';
import { api } from '../../helpers/request';
import FeatureForm from '../feature-form/FeatureForm';

export default class FeatureList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showForm: false,
            features: []
        }

        this.fetchFeatures();

        this.toggleForm = this.toggleForm.bind(this);
        this.createFeature = this.createFeature.bind(this);
    }

    fetchFeatures() {

        api('/features')
            .then(data => data.map(item => {item.id = item._id.$oid; return item;}))
            .then(data => { this.setState({ features: data })})
            .catch(error => toast.error('Fetch features data error !'));
    }

    notifiySucess() {
        toast.success('Feature is created successfully !');
    }

    notifiyError() {
        toast.error('Oops, an error occured.');
    }

    createFeature(name) {
        api('/features', 'POST', { name: name })
        .then(data => {
            data.id = data._id.$oid;
            return data;
        })
        .then(data => {
            const features = this.state.features;
            features.unshift(data);
            this.setState(features);
        })
        .then(this.notifiySucess)
        .then(this.toggleForm)
        .catch(this.notifiyError);
    }

    toggleForm() {
        this.setState({ showForm: !this.state.showForm })
    }

    render() {
        return (
            <Container className="FeatureList">
                <h2>All features</h2>
                <ButtonAdd toggleForm={() => this.toggleForm()} label='Create Feature'/>
                <Button onClick={() => this.fetchFeatures()} variant="link"><FontAwesomeIcon icon="sync"/></Button>
                {this.state.showForm ? <FeatureForm createFeature={this.createFeature} /> : null}
                <ListGroup as="ul" variant="flush">
                {this.state.features.map((feature, index) => <ListGroup.Item as="li"><Feature key={index} data={feature} /></ListGroup.Item>)}
                </ListGroup>
                <ToastContainer />
            </Container>
        )
    }
};
