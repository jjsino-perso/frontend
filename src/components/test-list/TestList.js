import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import { ListGroup } from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { api } from '../../helpers/request';
import ButtonAdd from '../button/ButtonAdd';
import TestForm from '../../components/test-form/TestForm';
import TestElement from '../../components/test-element/TestElement';

export default class TestList extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            showForm: false,
            feature: null,
            tests: []
        };

        this.toggleForm = this.toggleForm.bind(this);
        this.createTest = this.createTest.bind(this);
        this.updateStatus = this.updateStatus.bind(this);
        
    }

    componentDidMount() {
        const featureId = this.props.match.params.id
        if (featureId) {
            this.fetchFeature(featureId)
            this.fetchTests(featureId)
        }

    }
    
    fetchFeature(featureId) {
        return api(`/features/${featureId}`)
            .then(data => {
                data.id = data._id.$oid; 
                return data; 
            })
            .then(data => this.setState({ feature: data }) )
            .catch(error => {
                toast.error('Fetch feature data error !');
                return Promise.reject(error);
            });
    }

    fetchTests(featureId) {
        return api(`/features/${featureId}/tests`)
            .then( data => {
                return data.map(item => { item.id = item._id.$oid; return item; })
            })
            .then( data => this.setState({ tests: data}))
            .catch(error => {
                toast.error('Fetch tests data error !');
                return Promise.reject(error)
            })
    }

    toggleForm() {
        this.setState({showForm: !this.state.showForm})
    }

    createTest(name) {
        return api(`/features/${this.state.feature.id}/tests`, 'POST', {name: name})
            .then( data => {
                data.id = data._id.$oid; 
                return data;
            })
            .then(data => {
                const tests = this.state.tests;
                tests.unshift(data);
                this.setState({tests: tests});
            })
            .then(toast.success('Test created successfully'))
            .then(this.toggleForm)
            .catch(error => {
                toast.error('Create test error !');
                return Promise.reject(error)
            })
    }

    deleteTest(test) {
        return api(`/features/${this.state.feature.id}/tests/${test.id}`, 'DELETE')
            .then(() => {
                const tests = this.state.tests;
                const index = tests.findIndex(item => item.id === test.id);
                if (index >= 0) {
                    tests.splice(index, 1)
                }
                this.setState({tests: tests});
            })
            .then(
                toast.success('Test deleted successfully !')
            );
    }

    updateStatus(testId, testStatus) {
        return api(`/features/${this.state.feature.id}/tests/${testId}`, 'PUT', {status: testStatus})
            .then(() => {
                const tests = this.state.tests;
                const index = tests.findIndex(item => item.id === testId);
                if (index >= 0) {
                    const test = tests[index];
                    test.status = testStatus;
                    tests.splice(index, 1, test);
                }
                this.setState({tests: tests});
            })
            .then(toast.success('Test updated successfully !'))
            .catch(error => toast.error('An error occured!'))
    }


    render() {
        return (
            <div className="TestList">
                {this.state.feature ? <h2>Tests of { this.state.feature.name }</h2> : null }
                <Button variant="link"><Link to='/'><FontAwesomeIcon icon="undo"/> Return to features list</Link></Button>
                <ButtonAdd toggleForm={this.toggleForm} label='Create Test'/>
                {this.state.showForm ? <TestForm create={this.createTest} /> : null}
                <ul className="list-group list-group-flush">
                {this.state.tests.map((test, index) => <li className="list-group-item"><TestElement 
                        key={index} data={test}
                        deleteTest={test => this.deleteTest(test)}
                        updateStatus={this.updateStatus} /></li>)}
                </ul>
                <ToastContainer />
            </div>
        )
    }
};
