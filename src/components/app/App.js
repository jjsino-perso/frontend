import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import TestList from '../../components/test-list/TestList';
import FeatureList from '../../components/feature-list/FeatureList';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUndo, faSync } from '@fortawesome/free-solid-svg-icons';

import './App.scss';

library.add(faUndo);
library.add(faSync);

class App extends Component {
  render() {
    return (
      <div className="App">
        <Container>
          <Router>
            <div>
            <Route exact path='/' component={FeatureList}/>
            <Route exact path='/features' component={FeatureList}/>
            <Route path='/features/:id' component={TestList}/>
            </div>
          </Router>
        </Container>
      </div>
    );
  }
}

export default App;
