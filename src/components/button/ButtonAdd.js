import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import './ButtonAdd.scss';

export default class ButtonAdd extends Component {
   render () {
      return (
         <Button variant="primary" onClick={this.props.toggleForm} className="ButtonAdd">
            {this.props.label}
         </Button>
      )
   }
}