const ENDPOINT = 'http://localhost:3000';

const config = {
    headers: new Headers({ 'Content-Type': 'application/json' }),
    mode: 'cors',
    cache: 'default'
};


export function api(uri, method = 'GET', body = null, fetcher = window.fetch) {
    config.method = method;
    if (body !== null) config.body = JSON.stringify(body);
    return fetcher(
        `${ENDPOINT}${uri}`,
        config
    ).then(
        async response => {
            if (response.status >= 200 && response.status < 300) {
                const body = await response.text();
                try {
                    return JSON.parse(body);
                } catch (error) {
                    return body;
                }
            }
            throw response;
        },
        error => {
            throw error;
        },
    );
};
